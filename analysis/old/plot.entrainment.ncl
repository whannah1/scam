load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	conv_sch = (/"ras","ras_wtg"/)
	;conv_sch = (/"ras"/)
	
	case_name = "gate"


	;dir = (/"/maloney-scratch/whannah/CESM/scam_sim_2/"+conv_sch(0)+"_"+case_name+"/model_output/",\
	;	"/maloney-scratch/whannah/CESM/scam_sim_2/"+conv_sch(1)+"_"+case_name+"/model_output/"/)
	
	dir = (/"/maloney-scratch/whannah/CESM/scam_sim_2/"+conv_sch+"_"+case_name+"/model_output/"/)
	
	var = (/"RAS_E"/)
	
	;fig_file = "entrainment."+conv_sch(0)+".vs."+conv_sch(1)+"."+case_name
	
	temp = ""
	do d = 0,dimsizes(conv_sch)-1
	  temp = temp+conv_sch(d)+"."
	end do
	fig_file = "entrainment."+temp+case_name
	
	fig_type = "x11"
	
	lev_top = 7 ; ~ 80 hPa
	
	
  plot = new((/dimsizes(var),dimsizes(conv_sch)/),graphic)
	
  wks = gsn_open_wks(fig_type,fig_file)
  
  avg_dum = new((/2,30/),graphic)
  min_dum = new((/2,30/),graphic)
  max_dum = new((/2,30/),graphic)
  x25_dum = new((/2,30/),graphic)
  x75_dum = new((/2,30/),graphic)

do v = 0,dimsizes(var)-1
  do d = 0,dimsizes(conv_sch)-1

  files = systemfunc("ls "+dir(d))
  num_files = dimsizes(files)	
  
  do f = 0,num_files-1
  
    infile = addfile(dir(d)+files(f),"r")
    
    ;print("")
    ;print(""+dimsizes(infile->$var$))
  
  
    if f.eq.0 then
    
       dims = dimsizes(dimsizes(infile->$var(v)$))
       
       V = infile->$var(v)$(:,:,0,0)
       
       lev = infile->lev
       
    else 
    
      buff = infile->$var(v)$
      temp = V
        delete(V)
	
      sz1 = dimsizes(temp)
      sz2 = dimsizes(buff)
      sz3 = sz1
      sz3(0) = sz1(0)+sz2(0)
      V = new(sz3,float)
      
      V(0:sz1(0)-1,:) 	= temp
      V(sz1(0):,:)	= buff 
      
        delete(temp)
	delete(buff)
	delete(sz1)
	delete(sz2)
	delete(sz3)
      
    end if
    
  end do
  

  	V@_FillValue = -999.

	lambda_max = .2/50. 
	lambda_min = .2/2000.

	if var(v).eq."RAS_E" then
	  tsz = dimsizes(V)
	  do k = 0,tsz(1)-1
	    vals = ind(V(:,k).gt.lambda_max)
	    if .not.any(ismissing(vals)) then
	      V(vals,k) = -999.
	    end if
	    delete(vals)
	    vals = ind(V(:,k).lt.lambda_min)
	    if .not.any(ismissing(vals)) then
	      V(vals,k) = -999.
	    end if
	    delete(vals)
	  end do
	  delete(tsz)
	end if

  	res = True
	res@gsnDraw 	= False
	res@gsnFrame 	= False	
	res@gsnLeftString = conv_sch(d)+" "+var(v)
	res@trXMaxF = 0.0042
	
	res@gsnLeftStringFontHeightF  = .03
	;res@gsnRightStringFontHeightF = .03


	res@trYReverse 	= True	
	res@xyMarkLineMode    = "Markers" 
  	res@xyMarkers         =  3 
  	res@xyMarkerColor     = 1
  	res@xyMarkerSizeF     = 0.01
  	
  	avg_res = True
    	avg_res@gsMarkerIndex   = 16 
  	avg_res@gsMarkerSizeF   = 0.01
  	
  	min_res = True
    	min_res@gsMarkerIndex   = 11 
  	min_res@gsMarkerSizeF   = 0.005
  	min_res@gsMarkerColor 	= 89
  	
  	max_res = True
    	max_res@gsMarkerIndex   = 10 
  	max_res@gsMarkerSizeF   = 0.005
  	max_res@gsMarkerColor 	= 217
  	
  	x25_res = True
    	x25_res@gsMarkerIndex   = 6 
  	x25_res@gsMarkerSizeF   = 0.005
  	x25_res@gsMarkerColor 	= 89
  	
  	x75_res = True
    	x75_res@gsMarkerIndex   = 6 
  	x75_res@gsMarkerSizeF   = 0.005
  	x75_res@gsMarkerColor 	= 217
  	
	
    levsz = dimsizes(lev)
    Eavg_V = dim_avg(V(lev|:,time|:))
    Emin_V = dim_min(V(lev|:,time|:))
    Emax_V = dim_max(V(lev|:,time|:))
    
    
    timsz = dimsizes(V(0,:))
    x25_t = round(.25*timsz,3)-1
    x75_t = round(.75*timsz,3)-1
    
    plot(v,d) = gsn_csm_xy(wks,Eavg_V,lev,res)

    do k = 0,levsz(0)-1 
      V_sort = V(k,:)
      qsort(V_sort)
      Ex25_V = V_sort(x25_t)
      Ex75_V = V_sort(x75_t)
      avg_res@gsMarkerColor = 1 + k*8
      avg_dum(d,k) = gsn_add_polymarker(wks,plot(v,d),Eavg_V(k),lev(k),avg_res)
      min_dum(d,k) = gsn_add_polymarker(wks,plot(v,d),Emin_V(k),lev(k),min_res)
      max_dum(d,k) = gsn_add_polymarker(wks,plot(v,d),Emax_V(k),lev(k),max_res)
      ;x25_dum(d,k) = gsn_add_polymarker(wks,plot(v,d),Ex25_V,lev(k),x25_res)
      ;x75_dum(d,k) = gsn_add_polymarker(wks,plot(v,d),Ex75_V,lev(k),x75_res)
      delete(Ex25_V)
      delete(Ex75_V)
      delete(V_sort)
    end do
    
    delete(Eavg_V)
    delete(Emin_V)
    delete(Emax_V)
    delete(lev)
    delete(levsz)
    delete(avg_res)
    delete(min_res)
    delete(max_res)

  
  delete(V)
  delete(res)

  end do
  delete(files)
end do






  sz = dimsizes(plot)
  pplot = new(sz(0)*sz(1),graphic)
  
  do y = 0,sz(0)-1
    do x = 0,sz(1)-1
      ;print(y*sz(1)+x)
      pplot(y*sz(1)+x) = plot(y,x)
    end do
  end do

  gsn_panel(wks,pplot,(/dimsizes(var),2/),False)


end
