load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	conv_sch 	= (/"ras","rasle"/)
	case_name 	= "toga"	
	fmod 		= (/"",""/)
	version 	= "scam_sim_2"
	
	dir 	= (/"/Users/whannah/CESM/"+version+"/"+conv_sch+"_"+case_name+fmod+"/model_output/"/)
	obsDir  = "/Users/whannah/CESM/IOP_data/"

	var 	= (/"DTCOND","DCQ"/)
	obs_var = (/"q","T"/)
	
	fig_type = "x11"
	
	lev_top = 7;7+5 ; ~ 80 hPa
	
	
	if isStrSubset(case_name,"toga")
	  obsFile = "toga_coare.nc"
	  plot_diff = False
	end if
	if isStrSubset(case_name,"arm")
	  obsFile = "arm_iop.nc"
	  plot_diff = True
	end if
	if isStrSubset(case_name,"gate")
	  obsFile = "gate0874v1.2.nc"
	  plot_diff = False
	end if	
	
	fig_file = "profile.diff."+case_name+fmod(1)
	
	print("")
	print("  fig_file > "+fig_file)
	print("")
	
	if plot_diff then
	  cols = 2
	else
	  cols = 1
	end if
	
	
  plot = new((/dimsizes(var),cols/),graphic)
	
  wks = gsn_open_wks(fig_type,fig_file)
  
  color = (/90,225/)

do v = 0,dimsizes(var)-1


;-----------------------------------------------------------------
; Get Obs data
;-----------------------------------------------------------------
  infile = addfile(obsDir+obsFile,"r")   
  oV = infile->$obs_var(v)$(time|:,lev|:,lat|0,lon|0)  
    lev = infile->lev/100.

  tmp = oV
    delete(oV)
  oV = dim_avg_n(tmp,0)
    delete(tmp)
  
	oV!0 = "lev"
  	oV&lev = lev
	oV&lev@units = "hPa"
	oV&lev@positive = "down"
	
;-----------------------------------------------------------------
; Plot Obs data
;-----------------------------------------------------------------
	res = True
	res@gsnDraw 	= False
	res@gsnFrame 	= False	
	res@gsnLeftStringFontHeightF 	= .035
	res@gsnRightStringFontHeightF 	= .035
	res@gsnLeftString 		= case_name+":  "+var(v)+" model difference"
	res@gsnRightString 		= ""
	res@xyDashPattern 	= 0
	res@xyLineColor 	= "black"
	res@xyLineThicknessF	= 2.
	
	res@trYReverse = True
	
  ;plot(v,0) = gsn_csm_xy(wks,oV,lev,res)
    

;-----------------------------------------------------------------
; Get model data
;-----------------------------------------------------------------
  do d = 0,dimsizes(dir)-1 
  
    files = systemfunc("ls "+dir(d))
    num_files = dimsizes(files)	
  
  do f = 0,num_files-1
  
    infile = addfile(dir(d)+files(f),"r")

  
    if f.eq.0 then
    
       dims = dimsizes(dimsizes(infile->$var(v)$))

       
       if dims.eq.3 then
         V = infile->$var(v)$(:,:,:)
       end if
       
       if dims.eq.4 then
         V = infile->$var(v)$(:,lev_top:,:,:)
       end if

       
    else 
    
      buff = infile->$var(v)$
      temp = V
        delete(V)
	
      sz1 = dimsizes(temp)
      sz2 = dimsizes(buff)
      sz3 = sz1
      sz3(0) = sz1(0)+sz2(0)
      V = new(sz3,float)
      
      if dims.eq.2 then
        V(0:sz1(0)-1,:) 	= temp
	V(sz1(0):,:) 		= buff 
      end if
      if dims.eq.3 then
        V(0:sz1(0)-1,:,:) 	= temp
	V(sz1(0):,:,:) 	  	= buff 
      end if
      if dims.eq.4 then
        V(0:sz1(0)-1,:,:,:) 	= temp
	V(sz1(0):,:,:,:)	= buff 
      end if
      
        delete(temp)
	delete(buff)
	delete(sz1)
	delete(sz2)
	delete(sz3)
      
    end if
    
  
  end do
  
  
      temp = V
      delete(V)
      if dims.eq.2 then
         V = temp(:,0)
      end if
      if dims.eq.3 then
        V = temp(:,0,0)
      end if
      if dims.eq.4 then
        V = temp(:,:,0,0)
      end if
      delete(temp)
      
   
    tmp = V
      delete(V)
    V = dim_avg_n(tmp,0)
      delete(tmp)
   
   	V!0 = "lev"
   	V&lev = infile->lev(lev_top:)


 	; Interpolate model to obs pressure levels
	tmp = int2p_Wrap(V&lev,V,oV&lev,1)
	  delete(V)
	V = tmp
	  delete(tmp)

  	;res@xyLineColor = color(d)
  	
  	if var(v).eq."Q" then
  	  res@trXMaxF = 0.0016
  	  res@trXMinF = -0.0012
  	end if
  	if var(v).eq."T" then
  	  if case_name.eq."toga" then
    	    res@trXMaxF =  2.5
  	    res@trXMinF = -5.
  	  end if
  	  if case_name.eq."gate" then
    	    res@trXMaxF =  2.5
  	    res@trXMinF = -2.5
  	  end if
  	end if
  	
  if d.eq.1 then
    plot(v,0) = gsn_csm_xy(wks,V-iV,lev,res)
    lres = res
    lres@xyDashPattern  	= 1
    lres@xyLineThicknessF	= 1.
    lres@xyLineColor   		= "black"
    overlay(plot(v,0),gsn_csm_xy(wks,(/0,0/),(/1000,1/),lres))
    	lbres                    = True          ; labelbar only resources
        lbres@vpWidthF           = 0.1       ; labelbar width
        lbres@vpHeightF          = 0.05        ; labelbar height
        lbres@lbBoxMajorExtentF  = 0.3
        lbres@lbFillColors       = color(:)
        lbres@lbMonoFillPattern  = True          ; Solid fill pattern
        lbres@lbLabelFontHeightF = 0.015          ; font height. default is small
        lbres@lbLabelJust        = "CenterLeft"  ; left justify labels
    labels = conv_sch
    ;gsn_labelbar_ndc(wks,2,labels,0.54,0.91,lbres)
  
  else
    iV = V
  end if
      
      delete(V)

  end do	; dir
    delete(oV)
end do		; var


  sz = dimsizes(plot)
  pplot = new(sz(0)*sz(1),graphic)
  
  do y = 0,sz(0)-1
    do x = 0,sz(1)-1
      ;print(y*sz(1)+x)
      pplot(y*sz(1)+x) = plot(y,x)
    end do
  end do

	gsn_panel(wks,pplot,(/dimsizes(var),cols/),False)
	
end
