load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	ifile = (/"/maloney-scratch/whannah/CESM/scam_sim_2/zm_toga/model_output/zm_toga.cam5.h0.1992-12.nc",\
		"/maloney-scratch/whannah/CESM/IOP_data/toga_coare.nc"/)
	
	var  = (/"DIVT3D","DIVQ3D"/)
	var2 = (/"divT","divq"/)
	
	varx = (/"vertdivT","vertdivq"/)
	
	fig_file = "toga.div3d"
	
	fig_type = "x11"
	
	
  plot = new((/dimsizes(var),2/),graphic)
	
  wks = gsn_open_wks(fig_type,fig_file)

do v = 0,dimsizes(var)-1
  do d = 0,1
  
if False then 
  print("")
  print("  v = "+v)
  print("  d = "+d)
  print("")
  print("  ifile = "+ifile(d))
  print("")
end if
 
    infile = addfile(ifile(d),"r")
    
    if d.eq.0 then
      V = infile->$var(v)$(:,7:,:,:)
    else
      V = infile->$var2(v)$ 
      V = V + infile->$varx(v)$
    end if
  
  
      dims = dimsizes(dimsizes(V))
  
      temp = V
      delete(V)
      if dims.eq.2 then
         V = temp(:,0)
      end if
      if dims.eq.3 then
        V = temp(:,0,0)
      end if
      if dims.eq.4 then
        V = temp(:,:,0,0)
      end if
      delete(temp)
  
  
  
  	res = True
	res@gsnDraw 	= False
	res@gsnFrame 	= False	
	res@trYLog 	= False
	;res@trYAxisType = "LinearAxis"
	if d.eq.0 then
    	  res@gsnLeftString = var(v)
    	  
    	else
    	  ;res@trYLog = True
    	  res@gsnLeftString = var2(v)
    	end if
	
	
	res@gsnLeftStringFontHeightF  = .03
	res@gsnRightStringFontHeightF = .03
	
  if dims.eq.4 then
  
	res@cnLinesOn 	= False
	res@cnFillOn 	= True
	res@trYReverse 	= True
	res@lbLabelAngleF = -45.
	res@gsnSpreadColors = True
	
    plot(v,d) = gsn_csm_contour(wks,V(lev|:,time|:),res)
  
  else
  
    plot(v,d) = gsn_csm_y(wks,V(time|:),res)
  
  end if
  
    delete(V)
    delete(res)

  end do
end do

  sz = dimsizes(plot)
  pplot = new(sz(0)*sz(1),graphic)
  
  do y = 0,sz(0)-1
    do x = 0,sz(1)-1
      pplot(y*sz(1)+x) = plot(y,x)
    end do
  end do

  gsn_panel(wks,pplot,(/dimsizes(var),2/),False)


end
