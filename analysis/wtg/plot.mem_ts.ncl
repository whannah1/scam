; Created by Walter Hannah 3/29/2011
;
; This script reads model output for an ensemble of simulations
; with random perturbations to the initial conditions following
; Hack and Pedretti (2000). If plot_spagt is true then a spagetti
; plot will be created. If plot_histo is true then a time series 
; of a histogram will be created similar to Fig. 4 in Hack and
; Pedretti (2000).


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	case_name = "ras_wtg_sst_eq"
	
	dir = (/"/maloney-scratch/whannah/CESM/scam_sim_2/"+case_name+"/model_output/"/)
	
	var = (/"T","Q","DTCOND","DCQ"/)
	
	
	e = 10
	
	
	
	fig_file = "mem_ts."+case_name+"."+e
	
	fig_type = "x11"
	
	avg_per = 72 * 10	; 72 * days
	
	t1 = 0
	t2 = 7200-1	; 7200 total timesteps
	
  num_var = dimsizes(var)
  

  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(num_var,graphic)
  
  	res = True
  	res@gsnDraw = False
  	res@gsnFrame = False
  	res@cnFillOn = True
  	res@cnLinesOn = False
  	res@gsnSpreadColors = True
  	res@trYReverse = True

  	
  sub_dir = systemfunc("ls "+dir(0))
  num_mem = dimsizes(sub_dir)
  
  infile = addfile(dir+sub_dir(0)+"/"+case_name+"_000.cam5.h0.1995-07.nc","r")    
  lev = infile->lev
  
  sz = dimsizes(infile->$var$)
  tim_sz = sz(0)
  lev_sz = sz(1)
  
  V = new((/num_var,tim_sz,lev_sz/),float)
  
  ;sst = new(num_mem,float)
  
  ;--------------------------------------------------------------------------------------------
  ;--------------------------------------------------------------------------------------------
  do v = 0,num_var-1
    ;do e = 0,num_mem-1
    
      e_num = sprinti("%0.3i", e)
    
      infile = addfile(dir+sub_dir(e)+"/"+case_name+"_"+e_num+".cam5.h0.1995-07.nc","r")   
      
      ;if v.eq.0 then
      ;  sst(e) = infile->TS(0,0,0)-273.
      ;end if 
      
printVarSummary(infile->$var$)
printVarSummary(V)


      V(v,:,:) = (infile->$var(v)$(:,:,0,0))

        delete(infile)
      
    ;end do
    
      V!0 = "var"
      V!1 = "time"
      V!2 = "lev"
      ;V&sst = sst
      V&lev = lev
      
      	res@gsnLeftString = var(v)
      
    plot(v) = gsn_csm_contour(wks,V(var|v,lev|:,time|:),res)
  
  end do
  ;--------------------------------------------------------------------------------------------
  ;--------------------------------------------------------------------------------------------
  

    ;draw(plot)
    ;frame(wks)
    
    gsn_panel(wks,plot,(/2,num_var/2/),False)


  
end
