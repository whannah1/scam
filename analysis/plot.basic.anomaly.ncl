load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	conv_sch  = (/"raspe"/)
	
	case_name = "toga"
	
	fmod 	  = (/"_hack","_uw"/)
	;fmod 	  = (/"_0a","_0b","_0c"/)
	;fmod 	  = (/"_omega0","_omega2"/)

	fig_type  = "x11"

	dir = "/Users/whannah/CESM_SCM/scam_sim_3/"+conv_sch+"_"+case_name+fmod+"/model_output/"
	
	;var = (/"DTCOND"/)
	var = (/"T","Q","QRS","QRL","PRECC"/)

	obs_var = (/"T","q"/)
	
	fig_file = "basic."+conv_sch(0)+"."+case_name+".abc"
	
	
	lev_top = 6 ; ~ 80 hPa
		
    plot = new((/dimsizes(var),dimsizes(dir)/),graphic)
;=======================================================================================================
;=======================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"rainbow+gray")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")

  	obsDir  = "/Users/whannah/CESM_SCM/IOP_data/"
	if isStrSubset(case_name,"toga")
	  obsFile = "toga_iop_alt.nc"
	end if
	if isStrSubset(case_name,"arm")
	  obsFile = "arm_iop.nc"
	end if
	if isStrSubset(case_name,"gate")
	  obsFile = "gate0874v1.2.nc"
	end if
	if isStrSubset(case_name,"twp")
	  obsFile = "twp_iop.nc"
	end if	
	if isStrSubset(case_name,"ptest")
	  obsFile = "ptest_IOP.nc"
	end if
  
;=======================================================================================================
;=======================================================================================================
do v = 0,dimsizes(var)-1
  do d = 0,dimsizes(dir)-1
  files = systemfunc("ls "+dir(d))
  num_files = dimsizes(files)	
  do f = 0,num_files-1  
    infile = addfile(dir(d)+files(f),"r")    
    if f.eq.0 then
      print(infile)
       dims = dimsizes(dimsizes(infile->$var(v)$))       
       ;if dims.eq.2 then
       ;  V = infile->$var(v)$(:,:)
       ;end if
       if dims.eq.3 then
         V = infile->$var(v)$(:,:,:)
       end if
       if dims.eq.4 then
         V = infile->$var(v)$(:,lev_top:,:,:)
         ;V = infile->$var(v)$(:,:,:,:)
       end if
    else 
      buff = infile->$var(v)$
      temp = V
        delete(V)
      sz1 = dimsizes(temp)
      sz2 = dimsizes(buff)
      sz3 = sz1
      sz3(0) = sz1(0)+sz2(0)
      V = new(sz3,float)
      if dims.eq.2 then
        V(0:sz1(0)-1,:) 	= temp
	V(sz1(0):,:) 		= buff 
      end if
      if dims.eq.3 then
        V(0:sz1(0)-1,:,:) 	= temp
	V(sz1(0):,:,:) 	  	= buff 
      end if
      if dims.eq.4 then
        V(0:sz1(0)-1,:,:,:) 	= temp
	V(sz1(0):,:,:,:)	= buff(:,lev_top:,:,:)
      end if
        delete(temp)
	delete(buff)
	delete(sz1)
	delete(sz2)
	delete(sz3)
    end if
  end do
  ;--------------------------------------------------------
  ; reduce dimension size
  ;--------------------------------------------------------
      temp = V
      delete(V)
      if dims.eq.2 then
         V = temp(:,0)
      end if
      if dims.eq.3 then
        V = temp(:,0,0)
      end if
      if dims.eq.4 then
        V = temp(:,:,0,0)
      end if
      delete(temp)
  ;--------------------------------------------------------
  ; remove mean
  ;--------------------------------------------------------
    if dims.eq.4 then
      levsz = dimsizes(infile->lev(lev_top:))
      do k = 0,levsz-1
        V(:,k) = V(:,k) - avg(V(:,k))
      end do
    else
      ;V = V - avg(V)
    end if
  ;--------------------------------------------------------
  ;--------------------------------------------------------
  
  	V@_FillValue = -999.

	lambda_max = .2/50. 
	lambda_min = .2/2000.

	if var(v).eq."RAS_E" then
	  tsz = dimsizes(V)
	  do k = 0,tsz(1)-1
	    vals = ind(V(:,k).gt.lambda_max)
	    if .not.any(ismissing(vals)) then
	      V(vals,k) = -999.
	    end if
	    delete(vals)
	    vals = ind(V(:,k).lt.lambda_min)
	    if .not.any(ismissing(vals)) then
	      V(vals,k) = -999.
	    end if
	    delete(vals)
	  end do
	  delete(tsz)
	end if

  ;printVarSummary(V)
  ;printVarSummary(V&time)

  time = V&time 


  if var(v).eq."OMEGA" then
    tsz = dimsizes(V)
    print(V(tsz(0)-1,:))
    ;exit
  end if

	;-----------------------------
	; Get obs data
	;-----------------------------
	  ;infile = addfile(obsDir+obsFile,"r")
	  ;oV = infile->$obs_var(v)$(:,:,0,0)
	  ;lev = infile->lev
	  ;lev = lev/100.
	  ;lev@units = "hPa"
	  ;oV&lev = lev
	;-----------------------------
	;-----------------------------
	
  ;--------------------------------------------------------
  ;--------------------------------------------------------
	
  	res = True
	res@gsnDraw 	= False
	res@gsnFrame 	= False	
	res@gsnLeftString = conv_sch+" "+var(v)
	res@gsnYAxisIrregular2Linear = True
	;res@trYMinF = 100.
	;res@trYMaxF = 990.
	res@tiYAxisString = "Pressure"
	res@tiXAxisString = "Days"
	res@gsnLeftStringFontHeightF  = .03
	res@gsnRightStringFontHeightF = .03

	res@vpHeightF = 0.2
	
  if dims.eq.4 then
	  res@cnLinesOn 	= False
	  res@cnFillOn 	= True
	  res@trYReverse 	= True
	  ;res@lbLabelAngleF = -45.
	  res@lbOrientation = "Vertical"
	  res@gsnSpreadColors = True
	if var(v).eq."T" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF = -3.
	  res@cnMaxLevelValF =  3.
	  res@cnLevelSpacingF = .4
	end if	
	if var(v).eq."Q" then
	  V = V*1000.
	  V@units = "g/kg"
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF = -1.
	  res@cnMaxLevelValF =  1.
	  res@cnLevelSpacingF =  .5
	end if	
	if var(v).eq."DTCOND" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF  =-.0001
	  res@cnMaxLevelValF  = .0001
	  res@cnLevelSpacingF = .00005
	  ;res@cnFillMode = "RasterFill"
	end if	
	if var(v).eq."DCQ" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF  =-1.5e-7
	  res@cnMaxLevelValF  = 1.5e-7
	  res@cnLevelSpacingF = 0.5e-7
	end if
    plot(v,d) = gsn_csm_contour(wks,V(lev|:,time|:),res)
    ;	res@gsnLeftString = "Obs"
    ;plot(v,1) = gsn_csm_contour(wks,oV(lev|:,time|:),res)
  else
	res@trYReverse 	= False
	if var(v).eq."PRECC" then
	  V = V*60.*60.*24.*1000.
	  print(V(:10))
	  res@gsnRightString = "mm/day"
	  res@trYMaxF = 45.
	  res@trYMinF = 0.
	end if
	if var(v).eq."PRECL" then
	  V = V*60.*60.*24.*1000.
	  res@gsnRightString = "mm/day"
	  res@trYMaxF = 25.
	  res@trYMinF = 0.
	end if
	if var(v).eq."RASLE_scaling" then
	  res@trYMinF  = 0.7
	  res@trYMaxF  = 1.4
	end if
    plot(v,d) = gsn_csm_xy(wks,time,V(time|:),res)
    ;	res@gsnLeftString = "IOP Observations"
    ;plot(v,1) = gsn_csm_y(wks,oV(time|:),res)
  end if
    delete(V)
    ;delete(oV)
    delete(res)
  end do
    delete(files)
end do

;=======================================================================================================
;=======================================================================================================



;============================================================
; Combine plots
;============================================================

  sz = dimsizes(plot)
  pplot = new(sz(0)*sz(1),graphic)
  
  do y = 0,sz(0)-1
    do x = 0,sz(1)-1
      pplot(y*sz(1)+x) = plot(y,x)
    end do
  end do


  gsn_panel(wks,pplot,(/dimsizes(var),dimsizes(dir)/),False)



end
