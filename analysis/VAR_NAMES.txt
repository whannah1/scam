 Copyright (C) 1995-2011 - All Rights Reserved
 University Corporation for Atmospheric Research
 NCAR Command Language Version 6.0.0
 The use of this software is governed by a License Agreement.
 See http://www.ncl.ucar.edu/ for more details.
(0)	float ZMDICE      (time,lev,lat,lon)		Cloud ice tendency - Zhang-McFarlane convection		kg/kg/s
(0)	float Z3          (time,lev,lat,lon)		Geopotential Height (above sea level)		m
(0)	float WSUB        (time,lev,lat,lon)		Diagnostic sub-grid vertical velocity		m/s
(0)	float VV          (time,lev,lat,lon)		Meridional velocity squared		m2/s2
(0)	float VU          (time,lev,lat,lon)		Meridional flux of zonal momentum		m2/s2
(0)	float VT          (time,lev,lat,lon)		Meridional heat transport		K m/s
(0)	float VQ          (time,lev,lat,lon)		Meridional water transport		m/skg/kg
(0)	float VD01        (time,lev,lat,lon)		Vertical diffusion of Q		kg/kg/s
(0)	float V           (time,lev,lat,lon)		Meridional wind		m/s
(0)	float UU          (time,lev,lat,lon)		Zonal velocity squared		m2/s2
(0)	float U10         (time,lat,lon)		10m wind speed		m/s
(0)	float U           (time,lev,lat,lon)		Zonal wind		m/s
(0)	float TSMX        (time,lat,lon)		Maximum surface temperature over output period		K
(0)	float TSMN        (time,lat,lon)		Minimum surface temperature over output period		K
(0)	float TS          (time,lat,lon)		Surface temperature (radiative)		K
(0)	float TROP_Z      (time,lat,lon)		Tropopause Height		m
(0)	float TROP_T      (time,lat,lon)		Tropopause Temperature		K
(0)	float TROP_P      (time,lat,lon)		Tropopause Pressure		Pa
(0)	float TREFMXAV    (time,lat,lon)		Average of TREFHT daily maximum		K
(0)	float TREFMNAV    (time,lat,lon)		Average of TREFHT daily minimum		K
(0)	float TREFHT      (time,lat,lon)		Reference height temperature		K
(0)	float TOT_ICLD_VISTAU (time,lev,lat,lon)		Total in-cloud extinction visible sw optical depth		1
(0)	float TOT_CLD_VISTAU (time,lev,lat,lon)		Total gbx cloud extinction visible sw optical depth		1
(0)	float TMQ         (time,lat,lon)		Total (vertically integrated) precipitatable water		kg/m2
(0)	float TGCLDLWP    (time,lat,lon)		Total grid-box cloud liquid water path		kg/m2
(0)	float TGCLDIWP    (time,lat,lon)		Total grid-box cloud ice water path		kg/m2
(0)	float TGCLDCWP    (time,lat,lon)		Total grid-box cloud water path (liquid and ice)		kg/m2
(0)	float TDIFF       (time,lev,lat,lon)		difference from observed temp		K
(0)	float TAUY        (time,lat,lon)		Meridional surface stress		N/m2
(0)	float TAUX        (time,lat,lon)		Zonal surface stress		N/m2
(0)	float TAUTMSY     (time,lat,lon)		Meridional turbulent mountain surface stress		N/m2
(0)	float TAUTMSX     (time,lat,lon)		Zonal      turbulent mountain surface stress		N/m2
(0)	float T850        (time,lat,lon)		Temperature at 850 mbar pressure surface		K
(0)	float T700        (time,lat,lon)		Temperature at 700 mbar pressure surface		K
(0)	float T           (time,lev,lat,lon)		Temperature		K
(0)	float SWCF        (time,lat,lon)		Shortwave cloud forcing		W/m2
(0)	float SRFRAD      (time,lat,lon)		Net radiative flux at surface		W/m2
(0)	float SOLIN       (time,lat,lon)		Solar insolation		W/m2
(0)	float SNOWHLND    (time,lat,lon)		Water equivalent snow depth		m
(0)	float SNOWHICE    (time,lat,lon)		Water equivalent snow depth		m
(0)	float SHFLX       (time,lat,lon)		Surface sensible heat flux		W/m2
(0)	float SHDLFICE    (time,lev,lat,lon)		Detrained ice from shallow convection		kg/kg/s
(0)	float SFNUMLIQ    (time,lat,lon)		NUMLIQ surface flux		kg/m2/s
(0)	float SFNUMICE    (time,lat,lon)		NUMICE surface flux		kg/m2/s
(0)	float SFCLDLIQ    (time,lat,lon)		CLDLIQ surface flux		kg/m2/s
(0)	float SFCLDICE    (time,lat,lon)		CLDICE surface flux		kg/m2/s
(0)	float RHREFHT     (time,lat,lon)		Reference height relative humidity		fraction
(0)	float RELHUM      (time,lev,lat,lon)		Relative humidity		percent
(0)	float QRS         (time,lev,lat,lon)		Solar heating rate		K/s
(0)	float QRL         (time,lev,lat,lon)		Longwave heating rate		K/s
(0)	float QREFHT      (time,lat,lon)		Reference height humidity		kg/kg
(0)	float QFLX        (time,lat,lon)		Surface water flux		kg/m2/s
(0)	float QDIFF       (time,lev,lat,lon)		difference from observed water		kg/kg
(0)	float QC          (time,lev,lat,lon)		Q tendency - shallow convection LW export		kg/kg/s
(0)	float Q           (time,lev,lat,lon)		Specific humidity		kg/kg
(0)	float PSL         (time,lat,lon)		Sea level pressure		Pa
(0)	float PS          (time,lat,lon)		Surface pressure		Pa
(0)	float PRECT       (time,lat,lon)		Total (convective and large-scale) precipitation rate (liq + ice)		m/s
(0)	float PRECSL      (time,lat,lon)		Large-scale (stable) snow rate (water equivalent)		m/s
(0)	float PRECSH      (time,lat,lon)		Shallow Convection precipitation rate		m/s
(0)	float PRECSC      (time,lat,lon)		Convective snow rate (water equivalent)		m/s
(0)	float PRECR       (time,lat,lon)		total precip from RASPE convection		m/s
(0)	float PRECL       (time,lat,lon)		Large-scale (stable) precipitation rate (liq + ice)		m/s
(0)	float PRECCDZM    (time,lat,lon)		Convective precipitation rate from ZM deep		m/s
(0)	float PRECC       (time,lat,lon)		Convective precipitation rate (liq + ice)		m/s
(0)	float PHIS        (time,lat,lon)		Surface geopotential		m2/s2
(0)	float PCONVT      (time,lat,lon)		convection top  pressure		Pa
(0)	float PCONVB      (time,lat,lon)		convection base pressure		Pa
(0)	float PBLH        (time,lat,lon)		PBL height		m
(0)	float OMEGAT      (time,lev,lat,lon)		Vertical heat flux		K Pa/s
(0)	float OMEGA       (time,lev,lat,lon)		Vertical velocity (pressure)		Pa/s
(0)	float OCNFRAC     (time,lat,lon)		Fraction of sfc area covered by ocean		fraction
(0)	float NUMLIQ      (time,lev,lat,lon)		Grid box averaged cloud liquid number		kg/kg
(0)	float NUMICE      (time,lev,lat,lon)		Grid box averaged cloud ice number		kg/kg
(0)	float MPDICE      (time,lev,lat,lon)		CLDICE tendency - Morrison microphysics		kg/kg/s
(0)	float LWCF        (time,lat,lon)		Longwave cloud forcing		W/m2
(0)	float LHFLX       (time,lat,lon)		Surface latent heat flux		W/m2
(0)	float LANDFRAC    (time,lat,lon)		Fraction of sfc area covered by land		fraction
(0)	float IWC         (time,lev,lat,lon)		Grid box average ice water content		kg/m3
(0)	float ICWMR       (time,lev,lat,lon)		Prognostic in-cloud water mixing ratio		kg/kg
(0)	float ICIMR       (time,lev,lat,lon)		Prognostic in-cloud ice mixing ratio		kg/kg
(0)	float ICEFRAC     (time,lat,lon)		Fraction of sfc area covered by sea-ice		fraction
(0)	float FSUTOA      (time,lat,lon)		Upwelling solar flux at top of atmosphere		W/m2
(0)	float FSNTOAC     (time,lat,lon)		Clearsky net solar flux at top of atmosphere		W/m2
(0)	float FSNTOA      (time,lat,lon)		Net solar flux at top of atmosphere		W/m2
(0)	float FSNTC       (time,lat,lon)		Clearsky net solar flux at top of model		W/m2
(0)	float FSNT        (time,lat,lon)		Net solar flux at top of model		W/m2
(0)	float FSNSC       (time,lat,lon)		Clearsky net solar flux at surface		W/m2
(0)	float FSNS        (time,lat,lon)		Net solar flux at surface		W/m2
(0)	float FSDSC       (time,lat,lon)		Clearsky downwelling solar flux at surface		W/m2
(0)	float FSDS        (time,lat,lon)		Downwelling solar flux at surface		W/m2
(0)	float FREQZM      (time,lat,lon)		Fractional occurance of ZM convection		fraction
(0)	float FREQSH      (time,lat,lon)		Fractional occurance of shallow convection		fraction
(0)	float FREQS       (time,lev,lat,lon)		Fractional occurance of snow		fraction
(0)	float FREQR       (time,lev,lat,lon)		Fractional occurance of rain		fraction
(0)	float FREQL       (time,lev,lat,lon)		Fractional occurance of liquid		fraction
(0)	float FREQI       (time,lev,lat,lon)		Fractional occurance of ice		fraction
(0)	float FLUTC       (time,lat,lon)		Clearsky upwelling longwave flux at top of model		W/m2
(0)	float FLUT        (time,lat,lon)		Upwelling longwave flux at top of model		W/m2
(0)	float FLNTC       (time,lat,lon)		Clearsky net longwave flux at top of model		W/m2
(0)	float FLNT        (time,lat,lon)		Net longwave flux at top of model		W/m2
(0)	float FLNSC       (time,lat,lon)		Clearsky net longwave flux at surface		W/m2
(0)	float FLNS        (time,lat,lon)		Net longwave flux at surface		W/m2
(0)	float FLDS        (time,lat,lon)		Downwelling longwave flux at surface		W/m2
(0)	float FICE        (time,lev,lat,lon)		Fractional ice content within cloud		fraction
(0)	float DTV         (time,lev,lat,lon)		T vertical diffusion		K/s
(0)	float DTH         (time,lev,lat,lon)		T horizontal diffusive heating		K/s
(0)	float DTCOND      (time,lev,lat,lon)		T tendency - moist processes		K/s
(0)	float DPDLFICE    (time,lev,lat,lon)		Detrained ice from deep convection		kg/kg/s
(0)	float DCQ         (time,lev,lat,lon)		Q tendency due to moist processes		kg/kg/s
(0)	float CONCLD      (time,lev,lat,lon)		Convective cloud cover		fraction
(0)	float CMFMCDZM    (time,ilev,lat,lon)		Convection mass flux from ZM deep		kg/m2/s
(0)	float CMFMC       (time,ilev,lat,lon)		Moist shallow convection mass flux		kg/m2/s
(0)	float CMFDT       (time,lev,lat,lon)		T tendency - shallow convection		K/s
(0)	float CMFDQR      (time,lev,lat,lon)		Q tendency - shallow convection rainout		kg/kg/s
(0)	float CMFDQ       (time,lev,lat,lon)		QV tendency - shallow convection		kg/kg/s
(0)	float CLOUD       (time,lev,lat,lon)		Cloud fraction		fraction
(0)	float CLDTOT      (time,lat,lon)		Vertically-integrated total cloud		fraction
(0)	float CLDMED      (time,lat,lon)		Vertically-integrated mid-level cloud		fraction
(0)	float CLDLOW      (time,lat,lon)		Vertically-integrated low cloud		fraction
(0)	float CLDLIQ      (time,lev,lat,lon)		Grid box averaged cloud liquid amount		kg/kg
(0)	float CLDICESTR   (time,lev,lat,lon)		Stratiform CLDICE		kg/kg
(0)	float CLDICECON   (time,lev,lat,lon)		Convective CLDICE		kg/kg
(0)	float CLDICEBP    (time,lev,lat,lon)		CLDICE before physics		kg/kg
(0)	float CLDICEAP    (time,lev,lat,lon)		CLDICE after physics		kg/kg
(0)	float CLDICE      (time,lev,lat,lon)		Grid box averaged cloud ice amount		kg/kg
(0)	float CLDHGH      (time,lat,lon)		Vertically-integrated high cloud		fraction
(0)	float CLDFSNOW    (time,lev,lat,lon)		CLDFSNOW		1
(0)	float CDNUMC      (time,lat,lon)		Vertically-integrated droplet concentration		#/m2
(0)	float CCN3        (time,lev,lat,lon)		CCN concentration at S=0.1%		#/cm3
(0)	float AWNI        (time,lev,lat,lon)		Average cloud ice number conc		m-3
(0)	float AWNC        (time,lev,lat,lon)		Average cloud water number conc		m-3
(0)	float ATMEINT     (time,lat,lon)		Vertically integrated total atmospheric energy		J/m2
(0)	float AREL        (time,lev,lat,lon)		Average droplet effective radius		Micron
(0)	float AREI        (time,lev,lat,lon)		Average ice effective radius		Micron
(0)	float AQSNOW      (time,lev,lat,lon)		Average snow mixing ratio		kg/kg
(0)	float AQRAIN      (time,lev,lat,lon)		Average rain mixing ratio		kg/kg
(0)	float ANSNOW      (time,lev,lat,lon)		Average snow number conc		m-3
(0)	float ANRAIN      (time,lev,lat,lon)		Average rain number conc		m-3
(0)	float AEROD_v     (time,lat,lon)		Total Aerosol Optical Depth in visible band		1
(0)	integer nsteph      (time)		current timestep
(0)	double sol_tsi     (time)		total solar irradiance		W/m2
(0)	double f12vmr      (time)		f12 volume mixing ratio
(0)	double f11vmr      (time)		f11 volume mixing ratio
(0)	double n2ovmr      (time)		n2o volume mixing ratio
(0)	double ch4vmr      (time)		ch4 volume mixing ratio
(0)	double co2vmr      (time)		co2 volume mixing ratio
(0)	integer nscur       (time)		current seconds of current day
(0)	integer ndcur       (time)		current day (from base day)
(0)	double gw          (lat)		gauss weights
(0)	integer wnummax     (lat)		cutoff Fourier wavenumber
(0)	integer nlon        (lat)		number of longitudes
(0)	integer mdt         (ncl_scalar)		timestep		s
(0)	integer nbsec       (ncl_scalar)		seconds of base date
(0)	integer nbdate      (ncl_scalar)		base date (YYYYMMDD)
(0)	integer nsbase      (ncl_scalar)		seconds of base day
(0)	integer ndbase      (ncl_scalar)		base day
(0)	integer ntrk        (ncl_scalar)		spectral truncation parameter K
(0)	integer ntrn        (ncl_scalar)		spectral truncation parameter N
(0)	integer ntrm        (ncl_scalar)		spectral truncation parameter M
(0)	character time_written (time,chars)
(0)	character date_written (time,chars)
(0)	double time_bnds   (time,nbnd)		time interval endpoints
(0)	double lon         (lon)		longitude		degrees_east
(0)	double lat         (lat)		latitude		degrees_north
(0)	integer datesec     (time)		current seconds of current date
(0)	integer date        (time)		current date (YYYYMMDD)
(0)	double time        (time)		time		days since 1992-12-18 18:00:00
(0)	double P0          (ncl_scalar)		reference pressure		Pa
(0)	double hybi        (ilev)		hybrid B coefficient at layer interfaces
(0)	double hyai        (ilev)		hybrid A coefficient at layer interfaces
(0)	double ilev        (ilev)		hybrid level at interfaces (1000*(A+B))		level
(0)	double hybm        (lev)		hybrid B coefficient at layer midpoints
(0)	double hyam        (lev)		hybrid A coefficient at layer midpoints
(0)	double lev         (lev)		hybrid level at midpoints (1000*(A+B))		level
