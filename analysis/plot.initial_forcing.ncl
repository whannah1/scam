load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	conv_sch  = (/"raspe"/)
	
	case_name = "ptest"
	
	fmod 	  = (/"_0","_1"/)

	fig_type  = "x11"

	dir = "/Users/whannah/CESM_SCM/scam_sim_3/"+conv_sch+"_"+case_name+fmod+"/model_output/"
	
	var = (/"T","Q"/)

	obs_var = (/"T","q"/)
	
	fig_file = "basic."+conv_sch(0)+"."+case_name+".ptest"
	
	
	lev_top = 10 ; ~ 80 hPa

	num_d = dimsizes(dir)
	num_v = dimsizes(var)
		
    plot = new((/num_v,num_d/),graphic)
;=======================================================================================================
;=======================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"rainbow+gray")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")

  	obsDir  = "/Users/whannah/CESM_SCM/IOP_data/"
	if isStrSubset(case_name,"toga")
	  obsFile = "toga_iop_alt.nc"
	end if
	if isStrSubset(case_name,"arm")
	  obsFile = "arm_iop.nc"
	end if
	if isStrSubset(case_name,"gate")
	  obsFile = "gate0874v1.2.nc"
	end if
	if isStrSubset(case_name,"twp")
	  obsFile = "twp_iop.nc"
	end if	
	
	if isStrSubset(case_name,"ptest")
	  obsFile = "ptest/ptest_IOP_"+(/"00","05"/)+".nc"
	end if
  
;=======================================================================================================
;=======================================================================================================
do v = 0,num_v-1
  do d = 0,num_d-1
  
    files = systemfunc("ls "+dir(d))
    f = 0
    infile = addfile(dir(d)+files(f),"r")    
    lev = infile->lev(lev_top:)
    V = infile->$var(v)$(5,lev_top:,0,0)    
    ;--------------------------------------------------------
    ;--------------------------------------------------------
  
  	V@_FillValue = -999.

	;-----------------------------
	; Get obs data
	;-----------------------------
	;do d = 0,num_d-1
	  infile = addfile(obsDir+obsFile(d),"r")
	  ;if d.eq.0 then
	    olev = infile->lev
	    olev = olev/100.
	    olev@units = "hPa"
	    ;oV = new((/dimsizes(olev)/),float)
	    ;oV!0 = "case"
	    ;oV!1 = "lev"
	    ;oV&lev = olev
	  ;end if
	  oV = infile->$obs_var(v)$(12,:,0,0)
	;end do
	;-----------------------------
	;-----------------------------

	print(lev)
	print(olev)
	
  ;--------------------------------------------------------
  ;--------------------------------------------------------
	
  	res = True
	res@gsnDraw 	= False
	res@gsnFrame 	= False	
	res@gsnLeftString = conv_sch+" "+var(v)
	;res@gsnYAxisIrregular2Linear = True
	res@trYMinF = 200.
	res@trYMaxF = 1000.
	res@xyLineColor = "black"
	res@xyDashPattern = 0
	

	res@trYReverse 	= True

    plot(v,d) = gsn_csm_xy(wks,V,lev,res)
    ;	res@gsnLeftString = "IOP Observations"
    	res@xyLineColor = "red"
    	res@xyDashPattern = 1
    overlay( plot(v,d) , gsn_csm_xy(wks,oV,olev,res) )
    
    delete([/V,oV,res/])
  end do
    delete(files)
end do

;=======================================================================================================
;=======================================================================================================



;============================================================
; Combine plots
;============================================================

  sz = dimsizes(plot)
  pplot = new(sz(0)*sz(1),graphic)
  
  do y = 0,sz(0)-1
    do x = 0,sz(1)-1
      pplot(y*sz(1)+x) = plot(y,x)
    end do
  end do


  gsn_panel(wks,pplot,(/dimsizes(var),dimsizes(dir)/),False)



end
