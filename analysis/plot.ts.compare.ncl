load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"


begin

	conv_sch = (/"raspe","raspe"/)
	
	case_name = "arm"
	
	fmod 	= (/"_0","_1"/)


	dir = "/Users/whannah/CESM_SCM/scam_sim_3/"+conv_sch+"_"+case_name+fmod+"/model_output/"
	
	;var = (/"DTCOND"/)
	;var = (/"T","RASLE_scaling"/)
	var = (/"T","Q","PRECC"/)
	;var = (/"T","Q","RASLE_lam_slp"/)
	
	
	if dimsizes(dir).gt.1 then
	  fig_file = "TS."+conv_sch(0)+fmod(0)+".vs."+conv_sch(1)+fmod(1)+"."+case_name
	else
	  fig_file = "TS."+conv_sch(0)+"."+case_name+fmod(0)
	end if
	
	fig_type = "x11"
	
	lev_top = 7 ; ~ 80 hPa
	
  if dimsizes(conv_sch).eq.1 then	
    plot = new((/dimsizes(var),1/),graphic)
  else
    plot = new((/dimsizes(var),3/),graphic)
  end if
	
  wks = gsn_open_wks(fig_type,fig_file)

  ;gsn_define_colormap(wks,"rainbow+gray")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")

do v = 0,dimsizes(var)-1
  
  do d = 0,dimsizes(dir)-1

  files = systemfunc("ls "+dir(d))
  num_files = dimsizes(files)	
  
  do f = 0,num_files-1
  
    infile = addfile(dir(d)+files(f),"r")
    
    ;print("")
    ;print(""+dimsizes(infile->$var$))
  
  
    if f.eq.0 then
    
       dims = dimsizes(dimsizes(infile->$var(v)$))
       
       ;if dims.eq.2 then
       ;  V = infile->$var(v)$(:,:)
       ;end if
       
       if dims.eq.3 then
         V = infile->$var(v)$(:,:,:)
       end if
       
       
       if dims.eq.4 then
         ;V = infile->$var(v)$(:,lev_top:,:,:)
         V = infile->$var(v)$(:,:,:,:)
       end if

       
    else 
    
      buff = infile->$var(v)$
      temp = V
        delete(V)
	
      sz1 = dimsizes(temp)
      sz2 = dimsizes(buff)
      sz3 = sz1
      sz3(0) = sz1(0)+sz2(0)
      V = new(sz3,float)
      
      if dims.eq.2 then
        V(0:sz1(0)-1,:) 	= temp
	V(sz1(0):,:) 		= buff 
      end if
      if dims.eq.3 then
        V(0:sz1(0)-1,:,:) 	= temp
	V(sz1(0):,:,:) 	  	= buff 
      end if
      if dims.eq.4 then
        V(0:sz1(0)-1,:,:,:) 	= temp
	V(sz1(0):,:,:,:)	= buff 
      end if
      
        delete(temp)
	delete(buff)
	delete(sz1)
	delete(sz2)
	delete(sz3)
      
    end if
    
  
  end do
  
  
      temp = V
      delete(V)
      if dims.eq.2 then
         V = temp(:,0)
      end if
      if dims.eq.3 then
        V = temp(:,0,0)
      end if
      if dims.eq.4 then
        V = temp(:,:,0,0)
      end if
      delete(temp)
  
  
  	V@_FillValue = -999.

	lambda_max = .2/50. 
	lambda_min = .2/2000.

	if var(v).eq."RAS_E" then
	  tsz = dimsizes(V)
	  do k = 0,tsz(1)-1
	    vals = ind(V(:,k).gt.lambda_max)
	    if .not.any(ismissing(vals)) then
	      V(vals,k) = -999.
	    end if
	    delete(vals)
	    vals = ind(V(:,k).lt.lambda_min)
	    if .not.any(ismissing(vals)) then
	      V(vals,k) = -999.
	    end if
	    delete(vals)
	  end do
	  delete(tsz)
	end if

  	res = True
	res@gsnDraw 	= False
	res@gsnFrame 	= False	
	res@gsnLeftString = conv_sch(d)+"_"+case_name+"_"+fmod(d)+"   "+var(v)
	
	res@gsnLeftStringFontHeightF  = .03
	;res@gsnRightStringFontHeightF = .03
	
  if dims.eq.4 then
  
	res@cnLinesOn 	= False
	res@cnFillOn 	= True
	res@trYReverse 	= True
	res@lbLabelAngleF = -45.
	res@gsnSpreadColors = True
	
	;res@cnLevelSelectionMode = "AutomaticLevels"
	;res@cnMinLevelValF 	= -999.
	;res@cnMaxLevelValF 	= -999.
	;res@cnLevelSpacingF 	= -999.
	
	if var(v).eq."T" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF = 200.
	  res@cnMaxLevelValF = 300.
	  res@cnLevelSpacingF = 10.
	end if
	
	if var(v).eq."Q" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF = .002
	  res@cnMaxLevelValF = .018
	  res@cnLevelSpacingF = .002
	end if
	
	if var(v).eq."DTCOND" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF  =-.0002
	  res@cnMaxLevelValF  = .0002
	  res@cnLevelSpacingF = .00005
	end if
	
	if var(v).eq."DCQ" then
	  res@cnLevelSelectionMode = "ManualLevels"
	  res@cnMinLevelValF  =-1.5e-7
	  res@cnMaxLevelValF  = 1.5e-7
	  res@cnLevelSpacingF = 0.5e-7
	end if

	
    plot(v,d) = gsn_csm_contour(wks,V(lev|:,time|:),res)

  
    if d.eq.0 then
      iV = V
    else
      dV = V-iV
      copy_VarCoords(V,dV)
      dres = res
      dres@gsnLeftString = "Difference"
      dres@cnLevelSelectionMode = "AutomaticLevels"
      delete(dres@cnLevelSpacingF)
      	  ;dres@cnLevelSelectionMode = "ManualLevels"
	  ;dres@cnMinLevelValF  =-.00002
	  ;dres@cnMaxLevelValF  = .00002
	  ;dres@cnLevelSpacingF = .000005
	if var(v).eq."T" then
	  dres@cnLevelSelectionMode = "ManualLevels"
	  dres@cnMinLevelValF = -5.
	  dres@cnMaxLevelValF = 5.
	  dres@cnLevelSpacingF = 1.
	end if
	if var(v).eq."Q" then
	  dres@cnLevelSelectionMode = "ManualLevels"
	  dres@cnMinLevelValF = -.002
	  dres@cnMaxLevelValF =  .002
	  dres@cnLevelSpacingF = .0004
	end if
      plot(v,2) = gsn_csm_contour(wks,dV(lev|:,time|:),dres)
      delete(dV)
      delete(iV)
    end if
    
  else
  	
	res@trYReverse 	= False
	if var(v).eq."PRECC" then
	  V = V*60.*60.*24.*1000.
	  res@gsnRightString = "mm/day"
	  res@trYMaxF = 60.
	  res@trYMinF = 0.
	end if

	if var(v).eq."RASLE_scaling" then
	  res@trYMinF  = 0.7
	  res@trYMaxF  = 1.4
	end if
	
    plot(v,d) = gsn_csm_y(wks,V(time|:),res)
  
  
    if d.eq.0 then
      iV = V
      if .not.(dimsizes(conv_sch).eq.2) then
        delete(iV)
      end if
    else
      dV = V-iV
      copy_VarCoords(V,dV)
      dres = res
      dres@gsnLeftString = "Difference"
      delete(dres@trYMaxF)
      delete(dres@trYMinF)
      plot(v,2) = gsn_csm_y(wks,dV,dres)
      delete(dV)
      delete(iV)
      delete(dres)
    end if
  
  end if
  
  
  
  delete(V)
  delete(res)

  end do
  delete(files)
end do

  sz = dimsizes(plot)
  pplot = new(sz(0)*sz(1),graphic)
  
  do y = 0,sz(0)-1
    do x = 0,sz(1)-1
      ;print(y*sz(1)+x)
      pplot(y*sz(1)+x) = plot(y,x)
    end do
  end do

  if dimsizes(conv_sch).eq.1 then
    gsn_panel(wks,pplot,(/dimsizes(var),1/),False)
  else
    gsn_panel(wks,pplot,(/dimsizes(var),3/),False)
  end if


end
