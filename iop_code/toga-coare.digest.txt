Wed Jun 23 17:46:59 2010   ********toga_coare.nc*********
Dimension        0 named: ncl_scalar size:            1
Dimension        1 named: lon size:            1
Dimension        2 named: lat size:            1
Dimension        3 named: lev size:           38
Dimension        4 named: time size:           85
Var        0 name: bdate type: LONG
(ncl_scalar)
 --units:   yymmdd
 --long_name:   Base date as 6 digit integer
Var        1 name: lon type: FLOAT
(lon)
 --units:   degrees_east
 --long_name:   Longitude
Var        2 name: lat type: FLOAT
(lat)
 --units:   degrees_north
 --long_name:   Latitude
Var        3 name: lev type: FLOAT
(lev)
 --units:   Pa
 --long_name:   Pressure Levels
Var        4 name: tsec type: LONG
(time)
 --units:   s
 --long_name:   Time in seconds after 0Z on base date
Var        5 name: Year type: LONG
(time)
 --units:    
 --long_name:   Current year
Var        6 name: Month type: LONG
(time)
 --units:    
 --long_name:   Current month
Var        7 name: Day type: LONG
(time)
 --units:    
 --long_name:   Current calendar day
Var        8 name: phis type: FLOAT
(lon, lat)
 --long_name:   Surface Geopotential
 --units:   m2/s2
Var        9 name: Prec type: FLOAT
(lon, lat, time)
 --long_name:   Precipitation
 --units:   m/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       10 name: lhflx type: FLOAT
(lon, lat, time)
 --long_name:   Surface Latent Heat Flux
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       11 name: shflx type: FLOAT
(lon, lat, time)
 --long_name:   Surface Sensible Heat Flux
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       12 name: AreaPs type: FLOAT
(lon, lat, time)
 --long_name:   Area Mean Surface Pressure
 --units:   Pa
 --missing_value:        -999900.
 --_FillValue:        -999900.
Var       13 name: Ps type: FLOAT
(lon, lat, time)
 --long_name:   Surface Pressure (Central Facility)
 --units:   Pa
 --missing_value:        -999900.
 --_FillValue:        -999900.
Var       14 name: Tsair type: FLOAT
(lon, lat, time)
 --long_name:   Surface Air Temperature
 --units:   K
 --missing_value:        -9725.85
 --_FillValue:        -9725.85
Var       15 name: Tg type: FLOAT
(lon, lat, time)
 --long_name:   Tg Soil
 --units:   K
 --missing_value:        -9725.85
 --_FillValue:        -9725.85
Var       16 name: qg type: FLOAT
(lon, lat, time)
 --long_name:   Sfc Air q
 --units:   %
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       17 name: windsrf type: FLOAT
(lon, lat, time)
 --long_name:   Srf Wind Speed
 --units:   m/s
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       18 name: usrf type: FLOAT
(lon, lat, time)
 --long_name:   Srf U Wind
 --units:   m/s
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       19 name: vsrf type: FLOAT
(lon, lat, time)
 --long_name:   Srf V Wind
 --units:   m/s
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       20 name: SFC_LWdn type: FLOAT
(lon, lat, time)
 --long_name:   Srf LW Dn Rad
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       21 name: SFC_LWup type: FLOAT
(lon, lat, time)
 --long_name:   Srf LW Up Rad
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       22 name: SFC_SWdn type: FLOAT
(lon, lat, time)
 --long_name:   Srf SW Dn Rad
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       23 name: SFC_SWup type: FLOAT
(lon, lat, time)
 --long_name:   Srf SW Up Rad
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       24 name: TOA_SWdn type: FLOAT
(lon, lat, time)
 --long_name:   TOA SW dn
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       25 name: TOA_SWup type: FLOAT
(lon, lat, time)
 --long_name:   TOA SW up
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       26 name: TOA_LWup type: FLOAT
(lon, lat, time)
 --long_name:   TOA LW Up
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       27 name: dcolH2Odt type: FLOAT
(lon, lat, time)
 --long_name:   dColumn H2O_ dt
 --units:   m/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       28 name: colH20Adv type: FLOAT
(lon, lat, time)
 --long_name:   Column H2O Advection
 --units:   m/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       29 name: evapsrf type: FLOAT
(lon, lat, time)
 --long_name:   Srf Evaporation
 --units:   m/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       30 name: dcolDSEdt type: FLOAT
(lon, lat, time)
 --long_name:   dColumn Dry Static Energy_dt
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       31 name: dcolDSEAdt type: FLOAT
(lon, lat, time)
 --long_name:   Column Dry Static Energy Advection
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       32 name: colRH type: FLOAT
(lon, lat, time)
 --long_name:   Column Radiative Heating
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       33 name: colLH type: FLOAT
(lon, lat, time)
 --long_name:   Column Latent Heating
 --units:   W/m2
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       34 name: Prec_diag type: FLOAT
(lon, lat, time)
 --long_name:   Diagnosed Precipitation
 --units:   mm/hour
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       35 name: T type: FLOAT
(lon, lat, lev, time)
 --long_name:   Temperature
 --units:   K
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       36 name: q type: FLOAT
(lon, lat, lev, time)
 --long_name:   W.V. Mixing Ratio
 --units:   kg/kg
 --missing_value:        -9.99900
 --_FillValue:        -9.99900
Var       37 name: u type: FLOAT
(lon, lat, lev, time)
 --long_name:   U Windspeed
 --units:   m/s
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       38 name: v type: FLOAT
(lon, lat, lev, time)
 --long_name:   V Windspeed
 --units:   m/s
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       39 name: omega type: FLOAT
(lon, lat, lev, time)
 --long_name:   Vertical Pressure Velocity
 --units:   Pa/s
 --missing_value:        -277.750
 --_FillValue:        -277.750
Var       40 name: WD type: FLOAT
(lon, lat, lev, time)
 --long_name:   Wind_Div
 --units:   s-1
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       41 name: divT type: FLOAT
(lon, lat, lev, time)
 --long_name:   Horizontal T Advective Tendency
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       42 name: vertdivT type: FLOAT
(lon, lat, lev, time)
 --long_name:   Vertical T Advective Tendency
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       43 name: divq type: FLOAT
(lon, lat, lev, time)
 --long_name:   Horizontal Q Advective Tendency
 --units:   kg/kg/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       44 name: vertdivq type: FLOAT
(lon, lat, lev, time)
 --long_name:   Vertical Q Advective Tendency
 --units:   kg/kg/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       45 name: s type: FLOAT
(lon, lat, lev, time)
 --long_name:   Dry_Static_Energy
 --units:   K
 --missing_value:        -9999.00
 --_FillValue:        -9999.00
Var       46 name: divs type: FLOAT
(lon, lat, lev, time)
 --long_name:   Horizontal_s_Advec
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       47 name: vertdivs type: FLOAT
(lon, lat, lev, time)
 --long_name:   Vertical_s_Advec
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       48 name: ds_dt type: FLOAT
(lon, lat, lev, time)
 --long_name:   ds/dt
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       49 name: DT_dt type: FLOAT
(lon, lat, lev, time)
 --long_name:   DT/dt
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       50 name: Dq_dt type: FLOAT
(lon, lat, lev, time)
 --long_name:   Dq/dt
 --units:   kg/kg/s
 --missing_value:     -0.00277750
 --_FillValue:     -0.00277750
Var       51 name: Q1 type: FLOAT
(lon, lat, lev, time)
 --long_name:   Q1
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
Var       52 name: Q2 type: FLOAT
(lon, lat, lev, time)
 --long_name:   Q2
 --units:   K/s
 --missing_value:        -2.77750
 --_FillValue:        -2.77750
