; This script creates a stripped down version of the ARM IOP dataset
; for use in the NCAR SCAM

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"



begin

	tstep	= 20*60		 	; time step in seconds
	tsz 	= 365*60*60*24/tstep 	; number of time steps
	
	n_repeat = 1
	
	;print("")
	;print("  tstep = 	"+tstep+" sec	("+(tstep/60.)+" min)")
	;print("")
	;print("  tsz = 	"+tsz+" steps	("+(tsz/(60.*60.*24./tstep))+" days)")
	;print("")
	
	dir = "/maloney-scratch/whannah/CESM/IOP_data/"

  	ofile = "test_iop.nc"
	
	ifile = "arm_iop.nc"
	
	pi = 3.14159
	
	
  infile = addfile(dir+ifile,"r")

  if isfilepresent(dir+ofile) then
    system("rm "+dir+ofile)
  end if
  outfile = addfile(dir+ofile,"c")
  
        tsz = dimsizes(infile->tsec)*n_repeat
  
  	print("")
	print("  tsz = 	"+tsz+" steps	("+(tsz/(60.*60.*24./tstep))+" days)")
	print("")
  
  
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
  
  lon = 155.
  lon!0 	= "lon"
  lon@long_name = "Longitude"
  lon@units 	= "degrees_east"
  
  lat = 0.
  lat!0 	= "lat"
  lat@long_name = "Latitude"
  lat@units 	= "degrees_north"
  
  lev = (/3.54463800, 7.38881350, 13.967214, 23.944625, 	\
    	  37.2302900, 53.1146050, 70.059150, 85.4391150, 	\
    	  100.514695, 118.250335, 139.11539, 163.66207, 	\
	  192.539935, 226.513265, 266.48115, 313.501265, 	\
	  368.817980, 433.895225, 510.45525, 600.524200, 	\
	  696.79629,  787.702060,  867.16076, 929.648875, 	\
	  970.55483,  992.5561/)
  lev!0 	= "lev"
  lev@long_name = "Pressure Levels"
  lev@units 	= "Pa"
  
  
  bdate 	= 950101
  bdate@units	= "yymmdd"
  bdate!0	= "date"
  
  ;tsec		= new(floattoint(tsz),integer)  
  tsec 		= ispan(0,(tsz-1)*tstep,tstep)
  tsec!0	= "time"
  tsec@units	= "s"

 
  ;outfile->time = time
  ;outfile->lev   = lev
  ;outfile->lat   = lat
  ;outfile->lon   = lon
  ;outfile->bdate = bdate
  ;outfile->tsec	 = tsec
  
  
 

  
ivar=(/  "u",		"v",		"omega",	"lev"		\
	,"lhflx",	"shflx",	"Ps",		"Ptend"		\
	,"T",		"q"						\
	,"divT",	"divq",		"divs"				\
	,"DT_dt",	"dq_dt",	"ds_dt"				\
	,"Tg",		"Tsair",	"usrf",		"vsrf"		\
	,"TOA_LWup",	"TOA_SWdn",	"TOA_ins",	"NDRsrf"	\
	,"bdate",	"lon",		"lat"				\
	,"tsec"			\
	/)
  
  
  print("/////////////////////////////////////////////////")
    
  do v = 0,dimsizes(ivar)-1
  
  print(""+ivar(v))
  print("/////////////////////////////////////////////////")

    
    	sz 	= dimsizes(infile->$ivar(v)$)
    	dims 	= dimsizes(sz)
    	sz_n 	= sz
    	sz_n(0) = sz(0) * n_repeat
    	temp 	= new(sz_n,typeof(infile->$ivar(v)$))
	
    if (dims.eq.1).and.(ivar(v).ne."tsec") then
      delete(temp)
      temp = infile->$ivar(v)$
    else
    
    
      do n = 0,n_repeat-1
        t1 = sz(0)*n
        t2 = sz(0)*n+sz(0)-1
	if ivar(v).eq."tsec" then
	  if n.eq.0 then
	    temp(t1:t2) = infile->$ivar(v)$
	  else
	    div      = infile->$ivar(v)$(1) - infile->$ivar(v)$(0)
 	    last_val = infile->$ivar(v)$(sz(0)-1)
	    val_span = last_val - infile->$ivar(v)$(0) 
	    val1     = last_val + (val_span+div)*(n-1) +div
	    val2     = last_val + (val_span+div)*(n  ) 
            
	    temp(t1:t2)	    = ispan(val1,val2,div)
	      delete(div)
	      delete(last_val)
	      delete(val_span)
	      delete(val1)
	      delete(val2)
	  end if
        end if
        if dims.eq.2 then
          temp(t1:t2,:)	    = infile->$ivar(v)$(0:sz(0)-1,:)
        end if
        if dims.eq.3 then
          temp(t1:t2,:,:)   = infile->$ivar(v)$(0:sz(0)-1,:,:)
        end if
        if dims.eq.4 then
          temp(t1:t2,:,:,:) = infile->$ivar(v)$(0:sz(0)-1,:,:,:)
        end if
      end do
      
      copy_VarAtts(infile->$ivar(v)$,temp)
    
    end if
    
if ivar(v).eq."tsec" then
  if any( ( temp(1:sz_n-1)-temp(0:sz_n-2) ).ne.1200 ) then
    print("!!!!")
    print(ind( ( temp(1:sz_n-1)-temp(0:sz_n-2) ).ne.1200 ))
    print(temp(1259:1262))
    exit
  end if
end if

	buf = temp
	delete(temp)

    outfile->$ivar(v)$ = buf
    	
	delete(buf)
    
    
      
      delete(sz)
      delete(sz_n)
      delete(dims)
 
  end do
  
  
  
  
  
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
   
  
  
if False then
ivar=(/ "lon",		"lat",		"lev",		"tsec",		"bdate",	"phis",		\
	"Prec",		"lhflx",	"shflx",	"AreaPs",	"Ps",		"Tsair",	\
	"Tg",		"RH",		"windsrf",	"usrf",		"vsrf",		"NDRsrf",	\
	"TOA_LWup",	"TOA_SWdn",	"TOA_ins",	"lowcld",	"midcld",	"hghcld",	\
	"totcld",	"cldthk",	"cldht",	"cldliq",	"dcolH2Odt",	"colH2OAdv",	\
	"evapsrf",	"dcolDSEdt",	"dcolDSEAdt",	"colRH",	"colLH",	"Ptend",	\
	"T",		"q",		"u",		"v",		"omega",	"WD",		\
	"divT",		"vertdivT",	"divq",		"vertdivq",	"s",		"divs",		\
	"vertdivs",	"ds_dt",	"DT_dt",	"dq_dt",	"Q1",		"Q2"		/)
end if  
 
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
  
  
end
