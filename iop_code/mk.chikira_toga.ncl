; This script creates a stripped down version of the ARM IOP dataset
; for use in the NCAR SCAM

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"



begin

	dir = "/Users/whannah/CESM_SCM/IOP_data/"

  	ofile = "toga_chikira.nc"
	
	ifile = "toga_coare.nc"
	

	
	
  infile = addfile(dir+ifile,"r")

  if isfilepresent(dir+ofile) then
    system("rm "+dir+ofile)
  end if
  outfile = addfile(dir+ofile,"c")

  
        ;tsz = dimsizes(infile->tsec)
        tsz = 10
  
  
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
  
  vars_to_skip = (/"tsec","Day","Month","Year"/)
  vars_to_null = (/"omega","divT","divq","divs","vertdivT","vertdivq","vertdivs","DT_dt","Dq_dt","ds_dt",  \
  		   "Tg","Tsair","Q1","Q2","usrf","vsrf","colH20Adv","dcolH2Odt","dcolDSEAdt","dcolDSEdt",  \
  		   "colLH","colRH","TOA_LWup","TOA_SWup","TOA_SWdn","SFC_SWup","SFC_SWdn","SFC_LWup",      \
  		   "SFC_LWdn","u","v"/)

  
  infile = addfile(dir+ifile,"r")
  var = getfilevarnames(infile)
  
  lev = infile->lev
  lat = infile->lat
  lon = infile->lon
  
  
  prec_threshold1 = 20 / (60.*60.*24.*1000.)		; mm/day > m/s
  prec_threshold2 = 100 / (60.*60.*24.*1000.)		; mm/day > m/s
  
  vals = ind( (infile->Prec_diag(:,0,0).ge.prec_threshold1).and.(infile->Prec_diag(:,0,0).le.prec_threshold2) )
  ;=======================================================================
  ;=======================================================================
  do v = 0,dimsizes(var)-1
    ;------------------------------------------------------
    ;------------------------------------------------------
print("  "+var(v))
    if any(var(v).eq.vars_to_null) then
print("	= NULL")
      if dimsizes(dimsizes(infile->$var(v)$)).eq.3 then
        tmp_out = new((/tsz,1,1/),float)
         tmp_out(:,0,0) = 0.0
         tmp_out!0 = "time"
         tmp_out!1 = "lat"
         tmp_out!2 = "lon"
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
         delete(tmp_out)
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.4 then
        tmp_out = new((/tsz,dimsizes(lev),1,1/),float)
         do t = 0,tsz-1
          tmp_out(t,:,0,0) = 0.0
         end do
         tmp_out!0 = "time"
         tmp_out!1 = "lev"
         tmp_out!2 = "lat"
         tmp_out!3 = "lon"
         tmp_out&lev = lev
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
        delete(tmp_out)
      end if 
    ;------------------------------------------------------
    ;------------------------------------------------------
    else
    if .not.any(var(v).eq.vars_to_skip) then
      if dimsizes(dimsizes(infile->$var(v)$)).eq.1 then
        outfile->$var(v)$ = infile->$var(v)$
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.2 then
        tmp_out = infile->$var(v)$
        copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
         delete(tmp_out)
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.3 then
        tmp     = dim_avg_n(infile->$var(v)$(vals,  0,0),0)
        tmp_out = new((/tsz,1,1/),float)
         tmp_out(:,0,0) = tmp
         tmp_out!0 = "time"
         tmp_out!1 = "lat"
         tmp_out!2 = "lon"
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
         delete(tmp)
         delete(tmp_out)
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.4 then
        tmp     = dim_avg_n(infile->$var(v)$(vals,:,0,0),0)
        tmp_out = new((/tsz,dimsizes(lev),1,1/),float)
         do t = 0,tsz-1
          tmp_out(t,:,0,0) = tmp
         end do
         tmp_out!0 = "time"
         tmp_out!1 = "lev"
         tmp_out!2 = "lat"
         tmp_out!3 = "lon"
         tmp_out&lev = lev
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
;if var(v).eq."T" then
;  print(tmp_out(0,:,0,0))
;  wks = gsn_open_wks("x11","test")
;  res = False
;  print(infile->lev)
;  print(var)
;  plot = gsn_csm_y(wks,tmp_out(0,:,0,0),res)
;end if
        delete(tmp)
        delete(tmp_out)
      end if
      end if
    end if
  end do
  ;=======================================================================
  ;=======================================================================

;print(var)
  
  do i = 0,dimsizes(vars_to_skip)-1
    outfile->$vars_to_skip(i)$ = infile->$vars_to_skip(i)$(0:tsz-1)
  end do
  
  
  
  
  
end
